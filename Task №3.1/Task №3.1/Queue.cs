﻿using System;

namespace Task__3._1
{
    public class Queue
    {
        private Element _first;
        private Element _last;
        public int Count { get; private set; }
        public Queue()
        {
            Count = 0;
        }
        public void Enqueue(int value)
        {
            if (Count == 0)
            {
                AddFirst(value);
                return;
            }

            var newEnd = new Element
            {
                Previous = null,
                Value = value
            };
            _last.Previous = newEnd;
            _last = newEnd;
            Count++;
        }

        public int Dequeue()
        {
            var deletedValue = _first.Value;
            _first = _first.Previous;
            Count--;

            return deletedValue;
        }

        public int Peek()
        {
            return _first.Value;
        }

        private void AddFirst(int value)
        {
            _first = new Element
            {
                Value = value,
                Previous = null,
            };
            _last = _first;

            Count++;
        }
    }
}
