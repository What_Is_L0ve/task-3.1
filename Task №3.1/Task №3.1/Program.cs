﻿using System;

namespace Task__3._1
{            
    public class Program
    {
        static void Main(string[] args)
        {
            var stack = new Stack();
            var queue = new Queue();            

            var numbers = new int[] 
            {
                5, 
                10, 
                15, 
                20, 
                25 
            };            

            for (var i = 0; i < numbers.Length; i++)
            {
                queue.Enqueue(numbers[i]);
                stack.Push(numbers[i]);
            }
            Print(queue,stack);
        }

        public static void Print(Queue queue, Stack stack)
        {
            var getValue = "Получил значение: ";
            var removeValue = "Удалил значение: ";

            Console.WriteLine("Работа с Очередью");
            while (queue.Count > 0)
            {
                Console.WriteLine($"{getValue} {queue.Peek()}");
                Console.WriteLine($"{removeValue} {queue.Dequeue()}");               
            }

            Console.WriteLine("\nРабота со Стеком");
            while (stack.Count > 0)
            {
                Console.WriteLine($"{getValue} {stack.Peek()}");
                Console.WriteLine($"{removeValue} {stack.Pop()}");                
            }
        }
    }
}
