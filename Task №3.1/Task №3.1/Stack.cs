﻿using System;

namespace Task__3._1
{
    public class Stack
    {
        private Element _last;
        public int Count { get; private set; }
        public Stack()
        {
            Count = 0;
        }

        public void Push(int value)
        {
            if (Count == 0)
            {
                AddFirst(value);
                return;
            }

            var newEnd = new Element
            {
                Previous = _last,
                Value = value
            };
            _last = newEnd;
            Count++;
        }

        public int Pop()
        {
            var deletedValue = _last.Value;
            _last = _last.Previous;
            Count--;
            return deletedValue;
        }

        public int Peek()
        {
            return _last.Value;
        }

        private void AddFirst(int value)
        {
            _last = new Element
            {
                Value = value,
                Previous = null,
            };
            Count++;
        }
    }
}
