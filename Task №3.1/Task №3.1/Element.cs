﻿using System;

namespace Task__3._1
{
    public class Element
    {
        public Element Previous { get; set; }
        public int Value { get; set; }
    }
}
